/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.nostra13.universalimageloader.sample.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.nostra13.universalimageloader.sample.R;
import com.nostra13.universalimageloader.sample.example.EpgParser;
import com.nostra13.universalimageloader.sample.example.ListIsrael;
import com.nostra13.universalimageloader.sample.example.ListMyTv;
import com.nostra13.universalimageloader.sample.example.ListShared;
import com.nostra13.universalimageloader.sample.example.ListTorrentStream;
import com.nostra13.universalimageloader.sample.example.ListTorrentTv;
import com.nostra13.universalimageloader.sample.example.ParserGeneric;
import com.nostra13.universalimageloader.sample.example.ParserListener;
import com.nostra13.universalimageloader.sample.example.PlayListEntity;
import com.nostra13.universalimageloader.sample.example.PlayListEntityExtended;
import com.nostra13.universalimageloader.sample.example.PlayListManager;
import com.nostra13.universalimageloader.sample.example.UpdateApp;
import com.nostra13.universalimageloader.sample.example.VersionParser;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public class HomeActivity extends Activity implements ParserListener {


    protected Object lock;
    private List<ParserGeneric> listParsers = new ArrayList<>();
    private int playListCounter = 0;
    private boolean epgLoaded = false;
    int versionCode=-1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_home);

        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Log.e("LOG", "##HomeActivity : onCreate : versionCode : " + versionCode);

        new VersionParser (new ParserListener(){

            @Override
            public void parsingStarted(ParserGeneric.PlaySource id) {
                Log.e("LOG", "##HomeActivity actualVersion : parsingStarted");
            }

            @Override
            public void parsingFinished(ParserGeneric.PlaySource id, Object obj) {
                Log.e("LOG", "##HomeActivity actualVersion : parsingFinished");
                JSONObject actualVersionJson=(JSONObject)obj;
                final int ver=actualVersionJson.optInt("version");
                boolean mandatory=actualVersionJson.optBoolean("mandatory");

                Log.e("LOG", "##HomeActivity actualVersion : ver : " + ver + ", mandatory : "+mandatory);
                if (versionCode<ver && mandatory){
                    //do update (download new version)
                    Log.e("LOG", "##HomeActivity : MUST UPDATE BUILD");
                    try {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new AlertDialog.Builder(HomeActivity.this)
                                        .setTitle("We have a new version # " + ver)
                                        .setMessage("Let's download")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                SharedPreferences settings = getSharedPreferences("Categories", Context.MODE_PRIVATE);
                                                settings.edit().clear().commit();
                                                UpdateApp atualizaApp = UpdateApp.newInstance();
                                                atualizaApp.setContext(HomeActivity.this);
                                                atualizaApp.execute("https://bitbucket.org/ddobrinsky/tv/downloads/sample-release.apk", "5486033");

                                            }
                                        })

                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();

                            }
                        });

                    } catch (Exception e) {
                        Log.e("LOG", "NOOO : " + e);
                    }
                    return;
                }else{
                    //continue load flow
                    downloadEpg();
                }

            }

            @Override
            public void parsingError(ParserGeneric.PlaySource id, Exception e) {
                Log.e("LOG", "##HomeActivity actualVersion : parsingError");
            }
        }).parse();


    }


    private void downloadEpg(){
        Log.e("LOG", "##HomeActivity lets download EPG");
        PlayListManager.initInstance(this);
        epgLoaded = false;
        File localFile2 = getFileStreamPath("epg.db");
        if (!localFile2.exists()) {
            EpgParser epgParser = new EpgParser(this, this);
            epgParser.parse();
        } else {
            epgLoaded = true;
        }

//load all channels as Json

        listParsers.add(new ListMyTv(this)); // no logo in this list
        listParsers.add(new ListShared(this, this)); //no logo in this list (local list)
        listParsers.add(new ListTorrentStream(this)); // have logo full url
        listParsers.add(new ListTorrentTv(this)); // have logo
        listParsers.add(new ListIsrael(this, this));

        lock = this;
        playListCounter = 0;
        for (ParserGeneric entity : listParsers) {
            entity.parse();
        }
    }


    @Override
    public void parsingStarted(ParserGeneric.PlaySource id) {
        //Log.e("LOG", "##HomeActivity parsingStarted : " + id);

    }



    @Override
    public void parsingFinished(final ParserGeneric.PlaySource id, Object obj) {

        List<PlayListEntity> list=(List<PlayListEntity>)obj;
        //Log.e("LOG", "##HomeActivity parsingFinished : " + id);
        // here we got translated playlist from provider
        //Log.e("LOG", "lets filter IN programms , comes from " +id);

        if (id == ParserGeneric.PlaySource.EPG) {
            epgLoaded = true;
        } else {

            synchronized (lock) { // make sure that answer waits in queue
                StringBuffer buffIn=new StringBuffer();
                StringBuffer buffOut=new StringBuffer();
                buffOut.append("List from : " + id + "\n");
                for (PlayListEntity entity : list) { // run over list
                    int index = PlayListManager.getInstance().getInterestContainers().indexOf(entity); // check if it's
                    // favorite
                    // channel
                    // Log.e("LOG", "  " + entity.chanelName + " " +index);
                    if (index > -1) {
                        // index -1 mean that channel is not interesting
                       // Log.e("LOG", "IN : " + entity.getChannelName());
                        PlayListEntityExtended interestContainer = PlayListManager.getInstance().getInterestContainers().get(index);
                        interestContainer.addPlaySource(entity);
                       // buffIn.append("in : " + entity.getChannelName() + "\n");
                    } else {
                        //buffOut.append("out : " + entity.getChannelName() + "\n");
                        //Log.e("LOG", "##HomeActivity : OUT : " + entity.getChannelName());
                    }
                }

                runOnUiThread(new Runnable() {
                    public void run() {


                        if (id == ParserGeneric.PlaySource.ISRAEL_LIST) {
                            ImageView imageView = (ImageView) HomeActivity.this.findViewById(R.id.img1);
                            imageView.setImageResource(R.drawable.israel_launcher);
                        } else if (id == ParserGeneric.PlaySource.MY_TV) {
                            ImageView imageView = (ImageView) HomeActivity.this.findViewById(R.id.img2);
                            imageView.setImageResource(R.drawable.mytv2_launcher);
                        } else if (id == ParserGeneric.PlaySource.TORRENT_STREAM) {
                            ImageView imageView = (ImageView) HomeActivity.this.findViewById(R.id.img3);
                            imageView.setImageResource(R.drawable.torrentstream_launcher);

                        } else if (id == ParserGeneric.PlaySource.TORRENT_TV) {
                            ImageView imageView = (ImageView) HomeActivity.this.findViewById(R.id.img4);
                            imageView.setImageResource(R.drawable.torrent_tv_launcher);
                        } else if (id == ParserGeneric.PlaySource.SHARED) {
                            ImageView imageView = (ImageView) HomeActivity.this.findViewById(R.id.img5);
                            imageView.setImageResource(R.drawable.ic_stub);
                        }


                    }
                });
                Log.e("LOG", "##" + id +" channels out: " + buffOut);
                Log.e("LOG", "##" + id +" channels in: " + buffIn);
            }

        }

        if ((++playListCounter) == listParsers.size() && epgLoaded) {
            loadChoicesMap();
            startCustomActivity();
        }
        // Log.e("LOG", "FIRE  : SUCCESS : " + list.size() + " added");
    }

    @Override
    public void parsingError(final ParserGeneric.PlaySource id, Exception e) {
        //Log.e("LOG", "##HomeActivity parsingError : " +id);
        if (id == ParserGeneric.PlaySource.EPG) {
            epgLoaded = true;
        }
        if ((++playListCounter) == listParsers.size() && epgLoaded) {
            loadChoicesMap();
            startCustomActivity();
        }
        Log.e("LOG", "parsingError  : " + e);
    }

    private void loadChoicesMap() {
        try {
            File file = new File(getDir("data", MODE_PRIVATE), "map");
            FileInputStream f = new FileInputStream(file);

            ObjectInputStream s = new ObjectInputStream(f);
            Map<String, Integer> map = (HashMap<String, Integer>) s.readObject();
            s.close();
            for (PlayListEntityExtended entity : PlayListManager.getInstance().getInterestContainers()) {

                String name = entity.getChannelName();
                Integer choice = map.get(name);
                if (choice != null) {
                    entity.setActiveEntity(choice);
                }

            }
        } catch (Exception e) {
        }
    }


    void startCustomActivity(){
        Intent intent = new Intent(this, CustomActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        CustomActivity.activityStart=true;
        finish();
    }

    private void loadCategories(){
        SharedPreferences preferences = getSharedPreferences("Categories", MODE_PRIVATE);
        PlayListEntityExtended.Category[] categories=PlayListEntityExtended.Category.values();
        for(PlayListEntityExtended.Category category:categories) {
            boolean loadedBoolean= preferences.getBoolean(category.name(), category.isShown());
            category.setShown(loadedBoolean);

        }

    }
}