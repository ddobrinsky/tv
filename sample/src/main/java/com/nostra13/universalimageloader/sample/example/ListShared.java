package com.nostra13.universalimageloader.sample.example;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListShared extends ParserGeneric {
    private Context context;
    private List<PlayListEntity> playList = new ArrayList<PlayListEntity>();

    public ListShared(Context context, ParserListener parserListener) {
        super(parserListener);
        setUrls(new String[]{"https://bitbucket.org/ddobrinsky/tv/downloads/chanells.json"});
    }


    @Override
    public void parse() {
        new UpdateTask(urls[0]).execute();
    }


    @Override
    public PlaySource getID() {
        return PlaySource.SHARED;
    }


    private class UpdateTask extends AsyncTask<String, String, String> {


        private String url;


        protected UpdateTask(String url) {
            this.url = url;

        }

        private List<PlayListEntity> parseJson(JSONObject json) throws Exception {
            List<PlayListEntity> playList = new ArrayList<PlayListEntity>();

            JSONArray getArray = json.getJSONArray("channels");
            for(int i = 0; i < getArray.length(); i++){
                JSONObject ent = (JSONObject)getArray.get(i);
                //Log.e("LOG", "##ListShared : JSONObject : "+ent);
                String name=ent.getString("name");
                String link=ent.getString("link");
                PlayListEntity playListEntity = new PlayListEntity(getID(), name, link, null);
                playList.add(playListEntity);
            }
            return playList;
        }


        protected String doInBackground(String... urls) {

            try {
                if (ListShared.this.parserListener != null) {
                    ListShared.this.parserListener.parsingStarted(getID());
                }
                JSONObject json = readJsonFromUrl(this.url);
                List<PlayListEntity> list = parseJson(json);

                if (ListShared.this.parserListener != null) {
                    ListShared.this.parserListener.parsingFinished(getID(), list);
                }
               // Log.e("LOG", "##ListShared : got ListShared");
            } catch (Exception e) {
                Log.e("LOG", "##ListShared : ListShared fail " + e);
                if (ListShared.this.parserListener != null) {
                    ListShared.this.parserListener.parsingError(getID(), e);
                }
            }


            return null;
        }

    }

}
