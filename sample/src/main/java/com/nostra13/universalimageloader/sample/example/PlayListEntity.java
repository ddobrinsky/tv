package com.nostra13.universalimageloader.sample.example;

public class PlayListEntity {

    private String channelName;
    private String link;
    private String logo;
    private ParserGeneric onDemandParser = null;
    private ParserGeneric.PlaySource apkId;

    protected PlayListEntity() {

    }

    public PlayListEntity(ParserGeneric.PlaySource apkId, String chanelName, String link, String logo) {
        this.apkId = apkId;
        this.channelName = chanelName;
        this.link = link;
        this.logo = logo;
    }

    public ParserGeneric.PlaySource getApkId() {
        return apkId;
    }

    public void setApkId(ParserGeneric.PlaySource apkId) {
        this.apkId = apkId;
    }

    public String getChannelName() {
        return channelName;
    }

    public PlayListEntity setChannelName(String channelName) {
        this.channelName = new String(channelName);
        return this;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


    public String toString() {
        return "name : " + channelName + ", apkId : " + apkId + ", link : " + link;
    }

    public ParserGeneric getOnDemandParser() {
        return onDemandParser;
    }

    public void setOnDemandParser(ParserGeneric onDemandParser) {
        this.onDemandParser = onDemandParser;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof PlayListEntityExtended) {
            String[] chList = ((PlayListEntityExtended) obj).getChanelNameAlternative();

            for(String ch:chList){
                if (ch.toLowerCase().equals(channelName.toLowerCase())){
                    return true;
                }
            }


        }

        return false;
    }
}
