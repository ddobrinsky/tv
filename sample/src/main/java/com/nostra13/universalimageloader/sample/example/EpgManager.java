package com.nostra13.universalimageloader.sample.example;

import android.content.Context;

import java.util.List;

/**
 * Created by Dima_BI on 7/28/15.
 */
public class EpgManager implements ParserListener {

    private static EpgManager instance;
    private Context context;

    private EpgManager(List<PlayListEntityExtended> interestContainers) {


    }

    public static EpgManager initInstance(List<PlayListEntityExtended> interestContainers) {

        instance = new EpgManager(interestContainers);

        return instance;
    }

    public static EpgManager getInstance() {
        if (instance == null) {
            throw new NullPointerException("instance is null");
        }
        return instance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void parsingStarted(ParserGeneric.PlaySource id) {

    }

    @Override
    public void parsingFinished(ParserGeneric.PlaySource id, Object obj) {

    }

    @Override
    public void parsingError(ParserGeneric.PlaySource id, Exception e) {

    }
}
