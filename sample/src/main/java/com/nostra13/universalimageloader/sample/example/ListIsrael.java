package com.nostra13.universalimageloader.sample.example;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ListIsrael extends ParserGeneric {

    private Context context;

    public ListIsrael(Context context, ParserListener parserListener) {
        super(parserListener);
        this.context = context;
        setUrls(new String[]{"http://admin.applicaster.com/v12/accounts/69/channels/61.json?api[bundle]=com.applicaster.il.tenandroid&api[bver]=1.9.3&api[device_model]=GT-I9505&api[os_type]=android&api[os_version]=18&api[player]=NATIVE&api[sig]=7F84A28078DDF469D4A07C7FD3748BB6&api[timestamp]=1437854754&api[udid]=2bd60f87f729c341&api[uuid]=55b3eb5269702d0ff800e196&api[ver]=1.2"});
    }


    @Override
    public void parse() {
        new UpdateTask(urls[0]).execute();
    }

    @Override
    public PlaySource getID() {
        return PlaySource.ISRAEL_LIST;
    }

    private class UpdateTask extends AsyncTask<String, String, String> {
        private final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";

        private List<PlayListEntity> playList = new ArrayList<PlayListEntity>();
        private String url1;

        protected UpdateTask(String url1) {
            this.url1 = url1;
        }

        private List<PlayListEntity> sendGet() throws Exception {


            String apiHeadres[][] = {{"User-Agent", USER_AGENT}, {"Referer", "http://www.IsraeLIVE.org/"}};

            { // get chanel 10
                URL obj = new URL(this.url1);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                // optional default is GET
                con.setRequestMethod("GET");
                // add request header
                for (int i = 0; i < apiHeadres.length; i++) {
                    con.setRequestProperty(apiHeadres[i][0], apiHeadres[i][1]);
                }
                InputStreamReader reader = new InputStreamReader(con.getInputStream());
                BufferedReader in = new BufferedReader(reader);
                StringBuilder total = new StringBuilder();
                String line;

                while ((line = in.readLine()) != null) {
                    line = line.trim();
                    total.append(line);
                }

                JSONObject jsonObject = new JSONObject(total.toString());
                JSONObject data = jsonObject.getJSONObject("channel");
                String link = data.getString("stream_url");
                String name = data.getString("name");
                String logo = data.getString("default_image_url");
                playList.add(new PlayListEntity(getID(), name, link, logo));
                reader.close();
                in.close();
            }


            return playList;
        }

        protected String doInBackground(String... urls) {
            try {
                //System.err.println("##MyTvList : doInBackground : " + urls);
                ListIsrael.this.parserListener.parsingStarted(getID());
                List<PlayListEntity> list = sendGet();
                ListIsrael.this.parserListener.parsingFinished(getID(), list);
            } catch (Exception e) {
                ListIsrael.this.parserListener.parsingError(getID(), new Exception("##ListIsrael : " + e.getMessage()));

            }

            return null;
        }

    }


}
