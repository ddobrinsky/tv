package com.nostra13.universalimageloader.sample.example;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;


public class OnDemandParserTorrentTv extends ParserGeneric {

    public OnDemandParserTorrentTv() {
        super(null);

    }

    @Override
    public void parse() {
        new UpdateTask(urls[0]).execute();
    }

    @Override
    public PlaySource getID() {
        return PlaySource.TORRENT_TV;
    }

    private class UpdateTask extends AsyncTask<String, String, String> {

        private List<PlayListEntity> playList = new ArrayList<PlayListEntity>();
        private String url;

        protected UpdateTask(String url) {
            this.url = url;
        }

        private List<PlayListEntity> sendGet() throws Exception {
            Log.e("LOG", "##OnDemandParserTorrentTv : sendGet : url : " + url);
            String apiHeadres[][] = {{"User-Agent", USER_AGENT}, {"Connection", "Keep-Alive"}, {"Accept-Encoding", "gzip"}};

            // get torrent file
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            // add request header

            for (int i = 0; i < apiHeadres.length; i++) {
                con.setRequestProperty(apiHeadres[i][0], apiHeadres[i][1]);
            }
            //int responseCode = con.getResponseCode();
            //Log.e("LOG", "\nSending 'GET' request to URL : " + url);
            //Log.e("LOG", "Response Code : " + responseCode);

            GZIPInputStream gzis = new
                    GZIPInputStream(con.getInputStream());
            InputStreamReader reader = new InputStreamReader(gzis);
            BufferedReader in = new BufferedReader(reader);
            StringBuilder total = new StringBuilder();
            String line;

            while ((line = in.readLine()) != null) {
                line = line.trim();
                total.append(line);
            }

            JSONObject jsonObject = new JSONObject(total.toString());
            String source = jsonObject.getString("source");

            gzis.close();
            reader.close();
            in.close();

            playList.add(new PlayListEntity(getID(), null, source, null));
            return playList;
        }

        protected String doInBackground(String... urls) {
            if (OnDemandParserTorrentTv.this.parserListener != null) {
                try {
                    OnDemandParserTorrentTv.this.parserListener.parsingStarted(getID());

                    List<PlayListEntity> list = sendGet();
                    OnDemandParserTorrentTv.this.parserListener.parsingFinished(getID(), list);
                } catch (Exception e) {
                    OnDemandParserTorrentTv.this.parserListener.parsingError(getID(), e);
                }
            } else {
                Log.e("LOG", "##OnDemandParserTorrentTv : No listener : do nothing");
            }

            return null;
        }

    }

}
