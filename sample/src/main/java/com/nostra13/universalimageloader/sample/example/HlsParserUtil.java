/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.nostra13.universalimageloader.sample.example;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility methods for HLS manifest parsing.
 */
public final class HlsParserUtil {

    private static final String BOOLEAN_YES = "true";
    //private static final String BOOLEAN_NO = "false";

    private HlsParserUtil() {
    }

    public static String parseStringAttr(String line, Pattern pattern, String tag) {
        Matcher matcher = pattern.matcher(line);
        if (matcher.find() && matcher.groupCount() == 1) {
            return matcher.group(1);
        }
        return null;
    }

    public static int parseIntAttr(String line, Pattern pattern, String tag) {
        return Integer.parseInt(parseStringAttr(line, pattern, tag));
    }

    public static double parseDoubleAttr(String line, Pattern pattern, String tag) {
        return Double.parseDouble(parseStringAttr(line, pattern, tag));
    }

    public static String parseOptionalStringAttr(String line, Pattern pattern) {
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    public static boolean parseOptionalBooleanAttr(String line, Pattern pattern) {
        Matcher matcher = pattern.matcher(line);
        if (matcher.find() && matcher.groupCount() == 1) {
            String bool = matcher.group(1);

            return bool.equals(BOOLEAN_YES);
        }
        return false;
    }

    public static Pattern compileBooleanAttrPattern(String attrName) {
        return Pattern.compile(attrName + "=\"(.+?)\"");
    }

}