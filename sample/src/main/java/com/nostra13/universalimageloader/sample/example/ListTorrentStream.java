package com.nostra13.universalimageloader.sample.example;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class ListTorrentStream extends ParserGeneric {


    public ListTorrentStream(ParserListener parserListener) {
        super(parserListener);
        setUrls(new String[]{"https://acestreamcontroller2.appspot.com/secured/content/get-playlist?providerCode=0&version=1"});
    }

    @Override
    public void parse() {
        new UpdateTask(urls[0]).execute();
    }

    @Override
    public PlaySource getID() {
        return PlaySource.TORRENT_STREAM;
    }

    private class UpdateTask extends AsyncTask<String, String, String> {

        private List<PlayListEntity> playList = new ArrayList<PlayListEntity>();
        private String url;

        protected UpdateTask(String url) {
            this.url = url;
        }

        private List<PlayListEntity> sendGet() throws Exception {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj
                    .openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            // add request header
            con.setRequestProperty("X-APPLICATION-NAME", "ru.vidsoftware.acestreamcontroller.free");
            con.setRequestProperty("X-APPLICATION-VERSION", "89");
            con.setRequestProperty("X-APPLICATION-SIGNATURE", "c074a0fd2886c535326f3273fb71ba66c4047c50");
            con.setRequestProperty("X-LICENSED", "0");
            con.setRequestProperty("X-INSTALLATION-ID", "2bd60f87f729c341");
            con.setRequestProperty("X-LOCALE", "ru_RU");
            con.setRequestProperty("X-X-SDK-VERSION", "18");
            con.setRequestProperty("X-VENDOR-CODE", "public");
            con.setRequestProperty("Accept-Encoding", "gzip");
            con.setRequestProperty("User-Agent", "gzip");
            con.setRequestProperty("Connection", "Keep-Alive");
            //int responseCode = con.getResponseCode();
            //Log.e("LOG", "\nSending 'GET' request to URL : " + url);
            //Log.e("LOG", "Response Code : " + responseCode);

            GZIPInputStream gzis = new GZIPInputStream(con.getInputStream());
            InputStreamReader reader = new InputStreamReader(gzis);
            BufferedReader in = new BufferedReader(reader);
            //StringBuilder total = new StringBuilder();
            String line;
            StringBuffer playlistEntryString = new StringBuffer();
            while ((line = in.readLine()) != null) {
                //Log.e("LOG", "line : " + line);
                playlistEntryString.append(line);
            }

            JSONObject jsonObject = new JSONObject(playlistEntryString.toString());
            //JSONArray categories=(JSONArray)jsonObject.get("categories");
            JSONArray playlist = (JSONArray) jsonObject.get("playlist");
            for (int i = 0; i < playlist.length(); i++) {
                JSONObject entity = (JSONObject) playlist.get(i);
                String name = entity.getString("name");
                String link = entity.getString("uri");
                //String category=entity.getJSONArray("categories").getString(0);
                String logo = null;
                if (entity.optJSONObject("logo") != null) {
                    logo = entity.getJSONObject("logo").getString("uri");
                }

                playList.add(new PlayListEntity(getID(), name, link, logo));
            }
            gzis.close();
            reader.close();
            in.close();
            return playList;
        }

        protected String doInBackground(String... urls) {
            try {
                //System.err.println("##MyTvList : doInBackground : " + urls);
                ListTorrentStream.this.parserListener.parsingStarted(getID());
                List<PlayListEntity> list = sendGet();
                ListTorrentStream.this.parserListener.parsingFinished(getID(), list);
            } catch (Exception e) {
                ListTorrentStream.this.parserListener.parsingError(getID(), new Exception("##ListTorrentStream : " + e.getMessage()));
            }

            return null;
        }

    }

}
