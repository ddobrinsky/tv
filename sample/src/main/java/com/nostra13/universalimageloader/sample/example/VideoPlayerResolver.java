package com.nostra13.universalimageloader.sample.example;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.util.ArrayList;
import java.util.List;

public class VideoPlayerResolver {

    private static List<String> videoPlayers = new ArrayList<String>();
    private static List<String> acestreamPlayers = new ArrayList<String>();

    static {
        videoPlayers.add("com.mxtech.videoplayer.ad");
        videoPlayers.add("com.mxtech.videoplayer.pro");
        videoPlayers.add("org.videolan.vlc");


        acestreamPlayers.add("org.acestream.engine");
        acestreamPlayers.add("org.acestream");
        acestreamPlayers.add("ru.vidsoftware.acestreamcontroller.free");
    }

    private static boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static List<ResolveInfo> videoPlayers(Context context, String url) {
        String type = null;
        if (url.endsWith("acelive")) {

        } else {
            MimeTypeMap map = MimeTypeMap.getSingleton();
            String ext = MimeTypeMap.getFileExtensionFromUrl(url);
            type = map.getMimeTypeFromExtension(ext);
            // Log.e("#log", "ext : " + ext);
            // Log.e("#log", "type : " + type);

            if (type == null) {
                type = "application/x-mpegURL";
            }
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse(url);
        intent.setDataAndType(data, type);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PackageManager manager = context.getPackageManager();
        return manager.queryIntentActivities(intent, 0);
    }

    public static void launchVideoPlayer(final Activity context, final PlayListEntity playListEntity) {
        String linkToPlay = playListEntity.getLink();
        Log.e("LOG", "##VideoPlayerResolver : linkToPlay : " + linkToPlay + " , src " + playListEntity.getClass() + " " + playListEntity.getApkId());


        if (linkToPlay.endsWith("acelive") || linkToPlay.startsWith("acestream")) {
            //Log.e("LOG", "##VideoPlayerResolver : ace video link");
            if (isPackageInstalled("org.acestream", context)) {
                if (linkToPlay.endsWith("acelive")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri videoUri = Uri.parse(linkToPlay);//
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setDataAndType(videoUri, "acestream/*");
                    intent.setPackage("org.acestream");
                    context.startActivity(intent);
                }
                else{
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri videoUri = Uri.parse(linkToPlay);//
                    intent.setData(videoUri);
                    context.startActivity(intent);
                }
            } else {
                try {
                    context.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            new AlertDialog.Builder(context)
                                    .setTitle("No Ace Stream player")
                                    .setMessage("dowlnoad missing package ?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            UpdateApp atualizaApp = UpdateApp.newInstance();
                                            atualizaApp.setContext(context);
                                            atualizaApp.execute("https://bitbucket.org/ddobrinsky/tv/downloads/AcePlayer.apk", "41028454");
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();

                        }
                    });

                } catch (Exception e) {
                    Log.e("LOG", "NOOO : " + e);
                }
                return;
            }

        } else {
            //Log.e("LOG", "##VideoPlayerResolver : regular video link");
            if (isPackageInstalled("com.mxtech.videoplayer.pro", context)) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri videoUri = Uri.parse(linkToPlay);// "video/mpeg"
                //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(videoUri, "video/*");
                intent.setPackage("com.mxtech.videoplayer.pro");
                context.startActivity(intent);
            } else {
                {
                    try {
                        context.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new AlertDialog.Builder(context)
                                        .setTitle("No MX Pro")
                                        .setMessage("dowlnoad missing package ?")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                UpdateApp atualizaApp = UpdateApp.newInstance();
                                                atualizaApp.setContext(context);
                                                atualizaApp.execute("https://bitbucket.org/ddobrinsky/tv/downloads/MXPlayerPro.apk", "11727612");
                                            }
                                        })
                                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // do nothing
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();

                            }
                        });

                    } catch (Exception e) {
                        Log.e("LOG", "NOOO : " + e);
                    }
                    return;
                }
            }

        }


        // MimeTypeMap map = MimeTypeMap.getSingleton();
        // String ext = MimeTypeMap.getFileExtensionFromUrl(url);
        // String type = map.getMimeTypeFromExtension(ext);
        //
        // if (type == null)
        // type = "*/*";
        //
        // Intent intent = new Intent(Intent.ACTION_VIEW);
        // Uri data = Uri.parse(url);
        //
        // String pkg = ".TvdVideoActivity";
        // String cls = "com.softwinner.TvdVideo";
        // intent.setComponent(new ComponentName(pkg, cls));
        // intent.setDataAndType(data, type);
        //


//
//		Supported MIME types:
//		video/*
//		*/rmvb
//		*/avi
//		*/mkv
//		application/sdp
//		application/mp4
//		application/mpeg*
//		application/ogg
//		application/x-ogg
//		application/vnd.rn-realmedia*
//		application/3gpp*
//		application/vnd.3gp*
//		application/vnd.dvd*
//		application/vnd.dolby*
//		application/x-mpegURL
//		application/vnd.apple.mpegurl
//		application/x-quicktimeplayer
//		application/x-shockwave-flash

    }
}
