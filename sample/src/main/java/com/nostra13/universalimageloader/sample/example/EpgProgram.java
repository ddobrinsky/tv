package com.nostra13.universalimageloader.sample.example;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dima_BI on 8/12/15.
 */
public class EpgProgram {
    private static final String TAG = "EpgProgram";
    private String humanStart;
    private String humanStop;
    private String programTitle;
    private long start;
    private long stop;
    private SimpleDateFormat fullDateSimpleDateFormat = new SimpleDateFormat("d MMM, yyyy - HH:mm");

    public EpgProgram(String paramString, long paramLong1, long paramLong2) {

        this.programTitle = paramString;
        this.start = paramLong1;
        this.stop = paramLong2;
    }

    public EpgProgram(String paramString1, long paramLong1, long paramLong2, String paramString2, String paramString3) {
        this(paramString1, paramLong1, paramLong2);
        this.humanStart = paramString2;
        this.humanStop = paramString3;
    }

    public String getHumanStart() {
        return this.humanStart;
    }

    public String getHumanStop() {
        return this.humanStop;
    }

    public long getStart() {
        return this.start;
    }

    public long getStop() {
        return this.stop;
    }

    public String getTitle() {
        return this.programTitle;
    }

    @Override
    public String toString() {
        return "ProgramTitle: " + programTitle + ", start: " + fullDateSimpleDateFormat.format(new Date(start * 1000)) + ", stop:" + fullDateSimpleDateFormat.format(new Date(stop * 1000));
    }
}