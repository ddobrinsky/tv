package com.nostra13.universalimageloader.sample.example;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.sample.R;

import java.util.List;

public class CustomList extends BaseAdapter {

    private final Activity context;
    private List<PlayListEntityExtended> playListEntityExtendedList = null;
    private LayoutInflater inflater;

    public CustomList(Activity context) {
        //super(context, R.layout.list_single);
        this.context = context;
        playListEntityExtendedList = PlayListManager.getInstance().getInterestContainers();
    }

    @Override
    public int getCount() {
        return PlayListManager.getInstance().getInterestContainers().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {


        LayoutInflater inflater = context.getLayoutInflater();
        final PlayListEntityExtended playListEntityExtended = playListEntityExtendedList.get(position);
        //Log.e("LOG", "line : " + position + " : " +view);

        final View rowView = inflater.inflate(R.layout.rowview, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        txtTitle.setText(playListEntityExtended.getChannelName());

        TextView txtEpg = (TextView) rowView.findViewById(R.id.txt1);
        EpgProgram program = playListEntityExtended.getEpgProgram();
        ProgressBar progressBar = (ProgressBar) rowView.findViewById(R.id.progressBar);
        if (program != null) {
            txtEpg.setText(program.getTitle());
            progressBar.setVisibility(View.VISIBLE);
            long start = program.getStart();
            long stop = program.getStop();
            long now = System.currentTimeMillis() / 1000L;
            progressBar.setMax((int) (stop - start));
            progressBar.setProgress((int) (now - start));

            //Log.e("LOG", "##CustomList : program : " + program.getTitle() + " : " + (int) (now - start));
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        imageView.setImageResource(playListEntityExtended.getLogoId());

        RadioButtonWithLink[] sourceButtons = new RadioButtonWithLink[5];
        sourceButtons[0] = (RadioButtonWithLink) rowView.findViewById(R.id.img1);
        sourceButtons[1] = (RadioButtonWithLink) rowView.findViewById(R.id.img2);
        sourceButtons[2] = (RadioButtonWithLink) rowView.findViewById(R.id.img3);
        sourceButtons[3] = (RadioButtonWithLink) rowView.findViewById(R.id.img4);
        sourceButtons[4] = (RadioButtonWithLink) rowView.findViewById(R.id.img5);
        int indexActiveEntity = playListEntityExtended.getActiveEntity();
        for (int i = 0; i < 5; i++) {
            if (i < playListEntityExtended.getPlaySources().size()) {
                sourceButtons[i].setVisibility(View.VISIBLE);
                sourceButtons[i].setIndex(i);
                sourceButtons[i].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            int index = ((RadioButtonWithLink) compoundButton).getIndex();

                            playListEntityExtended.setActiveEntity(index);
                        }
                    }
                });


            } else {
                sourceButtons[i].setVisibility(View.INVISIBLE);
            }
        }

        try {
            sourceButtons[indexActiveEntity].setChecked(true);
        } catch (Exception e) {

        }


        return rowView;
    }
}