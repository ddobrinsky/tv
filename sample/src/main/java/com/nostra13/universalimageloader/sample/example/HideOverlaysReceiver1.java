package com.nostra13.universalimageloader.sample.example;

public class HideOverlaysReceiver1 extends HideOverlaysReceiver {
    @Override
    public void onHideOverlays(boolean hide) {
        if (hide) {
            // hide overlays, if any
        } else {
            // show previously hidden overlays, if any
        }
    }
}
