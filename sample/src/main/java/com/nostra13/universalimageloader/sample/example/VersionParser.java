package com.nostra13.universalimageloader.sample.example;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class VersionParser extends ParserGeneric {

    public VersionParser(ParserListener parserListener) {
        super(parserListener);
        setUrls(new String[]{"https://bitbucket.org/ddobrinsky/tv/downloads/version.json"});
    }

    @Override
    public void parse() {
        new UpdateTask(urls[0]).execute();
    }

    @Override
    public PlaySource getID() {
        return PlaySource.VERSION;
    }

    private class UpdateTask extends AsyncTask<String, String, String> {

        private List<PlayListEntity> playList = new ArrayList<PlayListEntity>();
        private String url;


        protected UpdateTask(String url) {
            this.url = url;

        }





        protected String doInBackground(String... urls) {

            try {
                if (VersionParser.this.parserListener != null) {
                    VersionParser.this.parserListener.parsingStarted(getID());
                }

                JSONObject json = readJsonFromUrl(this.url);

                if (VersionParser.this.parserListener != null) {
                    VersionParser.this.parserListener.parsingFinished(getID(), json);
                }
                Log.e("LOG", "##VersionParser : got Version");
            } catch (Exception e) {
                Log.e("LOG", "##VersionParser : Version fail " + e);
                if (VersionParser.this.parserListener != null) {
                    VersionParser.this.parserListener.parsingError(getID(), e);
                }
            }


            return null;
        }

    }

}
