package com.nostra13.universalimageloader.sample.example;

import android.util.Log;

public class CommandExecuter {




    public static  String startAdb(){
		String responseString = runSystemCommandAsRoot("adb devices");
		return responseString;
	}
	



	/**
	 * Runs a system command.
	 * 
	 * @param the_command_to_run
	 *            The system command to run.
	 * @return The result of the command.
	 */
	public static String runSystemCommand(String the_command_to_run) {
		String result = "";
		Log.e("init log :", "runSystemCommand : " + the_command_to_run);
		try {
			ShellCommand shell_command = new ShellCommand();
            ShellCommand.CommandResult command_result = shell_command.sh
                    .runWaitFor(the_command_to_run);

			if (!command_result.success()) {
				
				result = command_result.stderr;
			} else {
				result = command_result.stdout;
			}
		} catch (Exception e) {
			result="error at runSystemCommand : "+e;
			Log.e("init log :", "Error " + e.getMessage());
		}

		return result;
	}

	/**
	 * Runs a system command as root.
	 * 
	 * @param the_command_to_run
	 *            The system command to run as root.
	 * @return The result of the system command.
	 */
	public static String runSystemCommandAsRoot(String the_command_to_run) {
		String result = "";
		Log.e("init log :", "runSystemCommandAsRoot : " + the_command_to_run);
		try {
			ShellCommand shell_command = new ShellCommand();
			// If root commands are possible
			if (shell_command.canSU()) {
                ShellCommand.CommandResult command_result = shell_command.su.runWaitFor(the_command_to_run);

				if (!command_result.success()) {
					
					result = command_result.stderr;
				} else {
					result = command_result.stdout;
				}
			}
		} catch (Exception e) {
			result="error at runSystemCommandAsRoot : "+e;
			Log.e("init log :", "Error " + e.getMessage());
		}
		Log.e("init log :", "Error " + result);
		return result;
	}

}
