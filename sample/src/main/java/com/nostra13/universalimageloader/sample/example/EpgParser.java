package com.nostra13.universalimageloader.sample.example;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class EpgParser extends ParserGeneric {
    private Context context;

    public EpgParser(Context context, ParserListener parserListener) {
        super(parserListener);
        this.context = context;
        setUrls(new String[]{"http://inextapi.com/web/1/epg/getEPG2.php"});
    }

    @Override
    public void parse() {
        new UpdateTask(urls[0]).execute();
    }

    @Override
    public PlaySource getID() {
        return PlaySource.EPG;
    }

    private class UpdateTask extends AsyncTask<String, String, String> {

        private List<PlayListEntity> playList = new ArrayList<PlayListEntity>();
        private String url;
        private String postData;
        private String filename = "epg.db";
        private String tmpFilename = "tmpepg.db";

        protected UpdateTask(String url) {
            this.url = url;
            StringBuffer buffer = new StringBuffer();
            buffer.append("[");
            for (PlayListEntityExtended entity : PlayListManager.getInstance().getInterestContainers()) {
                buffer.append("\"" + entity.getChannelName() + "\",");
            }
            buffer.deleteCharAt(buffer.length() - 1);
            buffer.append("]");
            Log.e("LOG", "EpgManager : " + buffer);
            this.postData = buffer.toString();
        }

        private void getEpg() throws Exception {

            Log.e("EpgLoader", "EpgLoader: ");
            InputStream localInputStream = null;
            FileOutputStream localFileOutputStream = null;
            localFileOutputStream = context.openFileOutput(this.tmpFilename, 0);
            HttpURLConnection localHttpURLConnection = (HttpURLConnection) new URL(url).openConnection();
            localHttpURLConnection.setDoOutput(true);
            localHttpURLConnection.setDoInput(true);
            localHttpURLConnection.setFixedLengthStreamingMode(postData.getBytes().length);
            localHttpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            localHttpURLConnection.setRequestMethod("POST");
            BufferedOutputStream localBufferedOutputStream = new BufferedOutputStream(localHttpURLConnection.getOutputStream());
            localBufferedOutputStream.write(postData.getBytes());
            localBufferedOutputStream.flush();
            localInputStream = localHttpURLConnection.getInputStream();
            byte[] arrayOfByte = new byte[4096];

            while (true) {
                int i = localInputStream.read(arrayOfByte);
                if (i == -1)
                    break;
                if (isCancelled()) {
                    localInputStream.close();
                    throw new Exception("Stream cancelled");
                }
                localFileOutputStream.write(arrayOfByte, 0, i);
            }
            Log.e("EpgLoader", "close streams");
            if (localInputStream != null) {
                localInputStream.close();
            }
            if (localFileOutputStream != null) {
                localFileOutputStream.close();
            }
            Log.e("EpgLoader", "epg.db file downloaded");
            File localFile1 = context.getFileStreamPath(this.filename);
            File localFile2 = context.getFileStreamPath(this.tmpFilename);
            if (localFile1.exists())
                localFile1.delete();
            boolean bool = localFile2.renameTo(localFile1);
            Log.e("EpgLoader", "epg.db file replaced: " + bool);
        }

        protected String doInBackground(String... urls) {

            try {
                if (EpgParser.this.parserListener != null) {
                    EpgParser.this.parserListener.parsingStarted(getID());
                }
                getEpg();
                if (EpgParser.this.parserListener != null) {
                    EpgParser.this.parserListener.parsingFinished(getID(), null);
                }
                Log.e("LOG", "##EpgParser : got EPG");
            } catch (Exception e) {
                Log.e("LOG", "##EpgParser : EPG fail " + e);
                if (EpgParser.this.parserListener != null) {
                    EpgParser.this.parserListener.parsingError(getID(), e);
                }
            }


            return null;
        }

    }

}
