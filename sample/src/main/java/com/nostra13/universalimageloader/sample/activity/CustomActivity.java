package com.nostra13.universalimageloader.sample.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.sample.R;
import com.nostra13.universalimageloader.sample.example.CustomList;
import com.nostra13.universalimageloader.sample.example.EpgParser;
import com.nostra13.universalimageloader.sample.example.EpgProgram;
import com.nostra13.universalimageloader.sample.example.ParserGeneric;
import com.nostra13.universalimageloader.sample.example.ParserListener;
import com.nostra13.universalimageloader.sample.example.PlayListEntity;
import com.nostra13.universalimageloader.sample.example.PlayListEntityExtended;
import com.nostra13.universalimageloader.sample.example.PlayListManager;
import com.nostra13.universalimageloader.sample.example.UpdateApp;
import com.nostra13.universalimageloader.sample.example.VideoPlayerResolver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CustomActivity extends ActionBarActivity {
    static boolean active = false;
    protected Context lock;
    ListView list;
    EpgDbHelper epgDbHelper = new EpgDbHelper();
    Handler handler = new Handler();
    SimpleDateFormat fullDateSimpleDateFormat = new SimpleDateFormat("d MMM, yyyy - HH:mm");
    //boolean listRowPressed = false;
    boolean doubleBackToExitPressedOnce = false;

    public static boolean activityStart = false;
    private Runnable runnable = new Runnable() {
        public void run() {

            //Log.e("#log", "##CustomActivity : updateEpg");
            if (active) {
                updateEpg();
                list.invalidateViews();
            }
            handler.postDelayed(this, 1000 * 60 * 3);

        }
    };
    private ParserListener onDemandParserListener = new ParserListener() {

        @Override
        public void parsingStarted(ParserGeneric.PlaySource id) {


        }

        @Override
        public void parsingFinished(ParserGeneric.PlaySource id, Object obj) {
            List<PlayListEntity> list=(List<PlayListEntity>)obj;
            VideoPlayerResolver.launchVideoPlayer(CustomActivity.this, list.get(0));

        }

        @Override
        public void parsingError(ParserGeneric.PlaySource id, Exception e) {


        }

    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("#log", "##CustomActivity : activityStart " + CustomActivity.activityStart);
        if (!CustomActivity.activityStart) {
            startHomeActivity();
        }

        setContentView(R.layout.custom_main);

        lock = this;
        updateEpg();
        final CustomList adapter = new CustomList(CustomActivity.this);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener()

                                    {

                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view,
                                                                int position, long id) {

                                            // if (!listRowPressed) {
                                            //startImagePagerActivity(position);
                                            PlayListEntityExtended clicked = PlayListManager.getInstance().getInterestContainers().get(position);
                                            //Log.e("#LOG", "##CustomActivity : click :" + position);
                                            int indexActiveEntity = clicked.getActiveEntity();
                                            //Log.e("#LOG", "##CustomActivity : indexActiveEntity :" + indexActiveEntity+" , "+clicked.getPlaySources().size());
                                            PlayListEntity activeEntity =null;
                                            try{
                                                activeEntity = clicked.getPlaySources().get(indexActiveEntity);
                                            }catch (Exception e){
                                                Log.e("#LOG", "##CustomActivity : Exception :" + e);
                                            }

                                            //Log.e("#log","log 3");
                                            if (activeEntity == null) {
                                                Log.e("#LOG", "##CustomActivity : activeEntity : is null");
                                                return;
                                            }
                                            //Log.e("#LOG", "##CustomActivity " + position + ", activeEntity : " + activeEntity);
                                            ParserGeneric onDemandParser = activeEntity.getOnDemandParser();
                                            //Log.e("#log","log 4");
                                            if (onDemandParser != null) { // that means it's not direct link
                                                // , it requers additional
                                                // parsing
                                                //Log.e("#log", "this one needs ondemand parsing : " + activeEntity);
                                                onDemandParser.setUrls(new String[]{activeEntity.getLink()});
                                                onDemandParser.setParserListener(onDemandParserListener);
                                                onDemandParser.parse();
                                            } else {
                                                //Log.e("#log", "lets play video : " + activeEntity);
                                                VideoPlayerResolver.launchVideoPlayer(CustomActivity.this, activeEntity);

                                            }
                                            //     listRowPressed = true;
                                            //     new Handler().postDelayed(new Runnable() {

                                            //        @Override
                                            //         public void run() {
                                            //            listRowPressed = false;
                                            //        }
                                            //     }, 2000);
                                            // }
                                        }
                                    }

        );
        if (UpdateApp.getInstance() != null)
            UpdateApp.getInstance().setContext(this);

        handler.postDelayed(runnable, 1000 * 60 * 3);

    }

    @Override
    public void onStart() {
        super.onStart();
        //Log.e("#log", "##CustomActivity : onStart");
        active = true;
        updateEpg();
        list.invalidateViews();
    }

    @Override
    public void onStop() {
        super.onStop();
        //Log.e("#log", "##CustomActivity : onStop");
        active = false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        doVooDoo();
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {





        if (list.getSelectedView() == null) {
            return super.dispatchKeyEvent(keyEvent);
        }

        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT) {


            if (keyEvent.getAction() == KeyEvent.ACTION_UP) {

                RadioGroup radioGroup = (RadioGroup) list.getSelectedView().findViewById(R.id.appProviders);
                for (int j = 0; j < radioGroup.getChildCount(); j++) {
                    RadioButton radioButton = (RadioButton) radioGroup.getChildAt(j);
                    if (radioButton.isChecked()) {

                        if (j < radioGroup.getChildCount() - 1) {
                            ((RadioButton) radioGroup.getChildAt(j + 1)).setChecked(true);
                        }
                        break;
                    }
                }

            }
            return true;
        }
        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT) {
            if (keyEvent.getAction() == KeyEvent.ACTION_UP) {

                RadioGroup radioGroup = (RadioGroup) list.getSelectedView().findViewById(R.id.appProviders);
                for (int j = 0; j < radioGroup.getChildCount(); j++) {
                    RadioButton radioButton = (RadioButton) radioGroup.getChildAt(j);
                    if (radioButton.isChecked()) {

                        if (j > 0 && radioGroup.getChildAt(j - 1).getVisibility() == View.VISIBLE) {
                            ((RadioButton) radioGroup.getChildAt(j - 1)).setChecked(true);
                        }
                        break;
                    }
                }

            }
            return true;
        }

        return super.dispatchKeyEvent(keyEvent);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            saveChoicesMap();
            CustomActivity.activityStart = false;
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 1000);
    }

    private void updateEpg() {
        synchronized (lock) {
            //Log.e("EpgProvider", "##CustomActivity : updateEpg");
            epgDbHelper.updateEpgDbIfNeeded();
            SQLiteDatabase localSQLiteDatabase = epgDbHelper.open();

            HashMap localHashMap = new HashMap();
            try {
                String[] arrayOfString = new String[]{String.valueOf(System.currentTimeMillis() / 1000L)};
                Cursor localCursor = localSQLiteDatabase.rawQuery("SELECT c,t,s,e FROM epg WHERE ? BETWEEN s AND e", arrayOfString);
                localCursor.moveToFirst();
                while (localCursor.moveToNext()) {
                    String key = localCursor.getString(localCursor.getColumnIndex("c"));
                    Object value = new EpgProgram(localCursor.getString(localCursor.getColumnIndex("t")), localCursor.getLong(localCursor.getColumnIndex("s")), localCursor.getLong(localCursor.getColumnIndex("e")));
                    // Log.e("EpgProvider", "EpgProgram : "+value.toString());
                    localHashMap.put(key, value);
                }
            } catch (Exception localException) {
                Log.e("EpgProvider", "##CustomActivity : updateCurrentEpg! FAILED: " + localException);
            }


            try {
                for (PlayListEntityExtended entity : PlayListManager.getInstance().getInterestContainers()) {
                    String chName = entity.getChannelName();
                    EpgProgram program = (EpgProgram) localHashMap.get(chName);
                    entity.setEpgProgram(program);
                }
            }catch(Exception e){
                Log.e("EpgProvider", "##CustomActivity : EpgProgram! Exception: " + e);
            }
        }
    }


    private void saveCategories() {
        SharedPreferences preferences = getSharedPreferences("Categories", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        PlayListEntityExtended.Category[] categories = PlayListEntityExtended.Category.values();
        for (PlayListEntityExtended.Category category : categories) {
            String key = category.name();
            boolean shown = category.isShown();
            editor.putBoolean(key, shown);
        }
        editor.commit();
    }

    private void saveChoicesMap() {
        try {
            Map<String, Integer> map = new HashMap<>();
            for (PlayListEntityExtended entity : PlayListManager.getInstance().getInterestContainers()) {
                int activeIndex = entity.getActiveEntity();
                String name = entity.getChannelName();
                map.put(name, activeIndex);
            }
            File file = new File(getDir("data", MODE_PRIVATE), "map");
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
            outputStream.writeObject(map);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            Log.e("LOG", "##CustomActivity  : saveChoicesMap : Error : " + e);
        }
    }

    public class EpgDbHelper {
        private final String DB_NAME = "epg.db";
        private SQLiteDatabase mDataBase;

        public void downloadEpg() {
            // Log.e("EpgProvider", "##CustomActivity : downloadEpg");
            EpgParser epgParser = new EpgParser(CustomActivity.this, null);
            epgParser.parse();
        }

        public void updateEpgDbIfNeeded() {
            //Log.e("EpgProvider", "##CustomActivity : updateEpgDbIfNeeded");
            boolean doDownloadEpg = true;
            SQLiteDatabase localSQLiteDatabase = open();
            if (localSQLiteDatabase != null) ;
            try {
                Cursor localCursor = localSQLiteDatabase.rawQuery("SELECT v FROM settings WHERE n=?", new String[]{"epg_ttl"});
                if (localCursor != null) {
                    localCursor.moveToFirst();
                    long ttlTime = localCursor.getLong(localCursor.getColumnIndexOrThrow("v")) - (60 * 60);
                    long nowTime = System.currentTimeMillis() / 1000L;
                    String strTTL = fullDateSimpleDateFormat.format(new Date(ttlTime * 1000));
                    String strNow = fullDateSimpleDateFormat.format(new Date(nowTime * 1000));
                    //Log.e("EpgProvider", "##CustomActivity : EPG TTL is  : " + strTTL + " " + ttlTime);
                    //Log.e("EpgProvider", "##CustomActivity : EPG time now: " + strNow + " " + nowTime);

                    long diff = (ttlTime - nowTime) * 1000;
                    long diffSeconds = diff / 1000 % 60;
                    long diffMinutes = diff / (60 * 1000) % 60;
                    long diffHours = diff / (60 * 60 * 1000) % 24;
                    long diffDays = diff / (24 * 60 * 60 * 1000);
                    //Log.e("EpgProvider", "##CustomActivity : EPG DB update in: " + diffDays + " days, " + diffHours + " hours, " + diffMinutes + " minutes, " + diffSeconds + " seconds");


                    if (nowTime < ttlTime) {
                        doDownloadEpg = false;
                    }
                }
                if (doDownloadEpg) {
                    downloadEpg();
                } else {
                    // Log.e("EpgProvider", "##CustomActivity : No need to download DB");
                }


            } catch (Exception localException) {

                Log.e("EpgProvider", "##CustomActivity : updateEpgDbIfNeeded : Error " + localException);
            }
        }

        public boolean checkDataBase() {
            File localFile = CustomActivity.this.getFileStreamPath("epg.db");
            //Log.e("EpgProvider", "##CustomActivity : epg.db file exists? " + localFile.exists());
            return localFile.exists();
        }

        public void close() {
            try {
                //Log.e("EpgProvider", "##CustomActivity : EpgDbHelper close 1: " + mDataBase);
                if (mDataBase != null)
                    mDataBase.close();
                //Log.e("EpgProvider", "##CustomActivity : EpgDbHelper close 2: " + mDataBase);
                return;
            } finally {
            }
        }

        public SQLiteDatabase open() {
            if (mDataBase == null) {
                if (!checkDataBase())
                    return null;
                mDataBase = SQLiteDatabase.openOrCreateDatabase(CustomActivity.this.getFileStreamPath("epg.db"), null);
            }
            return mDataBase;
        }

        public SQLiteDatabase reopen() {
            mDataBase = null;
            return open();
        }
    }

    void startHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        CustomActivity.activityStart = true;
        finish();
    }


    private void doVooDoo() {
        final ArrayList<PlayListEntityExtended> customList = PlayListManager.getInstance().getAllChannels(this, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose channels (Must restart!!!)");

        //create an ArrayAdaptar from the String Array
        MyCustomAdapter dataAdapter = new MyCustomAdapter(this, R.layout.country_info, customList);
        ListView modeList = new ListView(this);
        // Assign adapter to ListView
        modeList.setAdapter(dataAdapter);
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener()

                                        {

                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view,
                                                                    int position, long id) {
                                                if (view != null) {
                                                    final ViewHolder holder = (ViewHolder) view.getTag();
                                                    runOnUiThread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            holder.name.performClick();
                                                        }
                                                    });

                                                }
                                            }
                                        }
        );

        builder.setView(modeList);
        final AlertDialog dialog = builder.create();
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Save", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                PlayListManager.getInstance().writeToFile(CustomActivity.this, customList);

                startHomeActivity();
            }
        });

        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {


            }
        });

        dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Reset list", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                PlayListManager.getInstance().deleteLocal(CustomActivity.this);
                startHomeActivity();

            }
        });
        dialog.show();
    }

    private class ViewHolder {
        TextView code;
        CheckBox name;
    }

    private class MyCustomAdapter extends ArrayAdapter<PlayListEntityExtended> {

        private ArrayList<PlayListEntityExtended> countryList;

        public MyCustomAdapter(Context context, int textViewResourceId, ArrayList<PlayListEntityExtended> countryList) {
            super(context, textViewResourceId, countryList);
            this.countryList = new ArrayList<PlayListEntityExtended>();
            this.countryList.addAll(countryList);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.country_info, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

                holder.name.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        //Log.e("AlertDialog", "##CustomActivity : onClick : " + v);
                        CheckBox cb = (CheckBox) v;
                        PlayListEntityExtended entity = (PlayListEntityExtended) cb.getTag();
                        entity.setShown(cb.isChecked());
                    }
                });
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            PlayListEntityExtended entity = countryList.get(position);

            holder.code.setText("");

            holder.name.setText(entity.getChannelName());
            holder.name.setChecked(entity.isShown());
            holder.name.setTag(entity);

            return convertView;

        }

    }

    private void checkButtonClick() {


    }


}
