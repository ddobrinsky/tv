package com.nostra13.universalimageloader.sample.example;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dima_BI on 7/28/15.
 */
public class PlayListManager {

    private static PlayListManager instance;
    private ArrayList <PlayListEntityExtended> interestContainers;
    private List<ParserGeneric> listParsers = new ArrayList<ParserGeneric>();


    public ArrayList <PlayListEntityExtended>  getAllChannels(Context context,boolean onlyShown){
        ArrayList <PlayListEntityExtended> interest = new ArrayList<PlayListEntityExtended>();
        String localJson = null;
        boolean localJsonExists = false;
        try {
            localJson = readFromFile(context);
            localJsonExists = true;
        } catch (IOException e) {
            localJsonExists = false;
        }

        if (!localJsonExists) {

            try {

                StringBuffer builder = new StringBuffer();

                InputStream rawChannels = context.getAssets().open("channels.json");//load raw /assets/json file
                BufferedReader bReader = new BufferedReader(new InputStreamReader(rawChannels));

                String line;
                while ((line = bReader.readLine()) != null) {
                    builder.append(line);
                }
                localJson = builder.toString();


            } catch (Exception e) {
                Log.e("LOG", "entity  : Exception : " + e);
            }
        }
        try {
            //save json string to local
            JSONArray json = new JSONArray(localJson);

            for (int i = 0; i < json.length(); i++) {
                JSONObject channel = (JSONObject) json.get(i);
                String mDrawableName = channel.getString("logo");
                int resID = context.getResources().getIdentifier(mDrawableName, "drawable", context.getPackageName());
                JSONArray channelNames = channel.getJSONArray("channelNames");
                boolean addToList=channel.getBoolean("shown");


                if(onlyShown) {
                    if (addToList) {
                        interest.add(new PlayListEntityExtended(resID, true, getStringArray(channelNames)));
                    }
                }else{
                    interest.add(new PlayListEntityExtended(resID, addToList, getStringArray(channelNames)));
                }
            }
        }catch (Exception e){
            Log.e("LOG", "entity  : Exception : " + e);
        }
        return interest;
    }

    private PlayListManager(Context context) {
        interestContainers=getAllChannels(context,true);
    }

    public static String[] getStringArray(JSONArray jsonArray) {
        String[] stringArray = null;
        int length = jsonArray.length();
        if (jsonArray != null) {
            stringArray = new String[length];
            for (int i = 0; i < length; i++) {
                stringArray[i] = jsonArray.optString(i);
            }
        }
        return stringArray;
    }

    public static PlayListManager initInstance(Context context) {
        instance = new PlayListManager(context);

        return instance;
    }

    public static PlayListManager getInstance() {
        if (instance == null) {
            throw new NullPointerException("instance is null");
        }
        return instance;
    }


    public void deleteLocal(Context context){
        File localFile1 = context.getFileStreamPath("channels.json");
        if (localFile1.exists())
            localFile1.delete();
    }

    public void writeToFile(Context context,ArrayList<PlayListEntityExtended> list) {
        try {
            String data=toJson(list).toString();
            //save  to /data/channels.json
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("channels.json", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String readFromFile(Context context) throws IOException {

        String ret = "";

        InputStream inputStream = context.openFileInput("channels.json"); //read from /data/channels.json

        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }

            inputStream.close();
            ret = stringBuilder.toString();
        }
        return ret;
    }

    public ArrayList <PlayListEntityExtended> getInterestContainers() {
        return interestContainers;
    }

    public JSONArray toJson(ArrayList<PlayListEntityExtended> list) {
        JSONArray jsonArray = new JSONArray();
        try {

            for (PlayListEntityExtended entity : list) {
                jsonArray.put(entity.toJson());
            }
        } catch (Exception e) {
            Log.e("LOG", "entity  : Exception : " + e);
        }
        return jsonArray;
    }
}
