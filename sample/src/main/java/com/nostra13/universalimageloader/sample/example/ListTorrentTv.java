package com.nostra13.universalimageloader.sample.example;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class ListTorrentTv extends ParserGeneric {


    public ListTorrentTv(ParserListener parserListener) {
        super(parserListener);
        setUrls(new String[]{
                //http://api.torrent-tv.ru/v3/auth.php?guid=a477c0a7-4162-45c2-8578-f0ea37f6795d&password=anonymous&application=android
                "http://api.torrent-tv.ru/v3/auth.php?guid=5d6a19ff-d6f5-49dc-9f1b-3f35ce1d2a49&password=Qazxsw&application=android&username=dmitry.dobrinsky%40gmail.com",
                "http://api.torrent-tv.ru/v3/translation_list.php?type=channel&session=#SESSIOSN#&typeresult=json"});
    }

    @Override
    public void parse() {
        new UpdateTask(urls[0], urls[1]).execute();
    }

    @Override
    public PlaySource getID() {
        return PlaySource.TORRENT_TV;
    }


    private class UpdateTask extends AsyncTask<String, String, String> {
        private final String USER_AGENT = "Dalvik/1.6.0 (Linux; U; Android 4.3; GT-I9505 Build/JSS15J)";

        private List<PlayListEntity> playList = new ArrayList<PlayListEntity>();
        private String url1;
        private String url2;

        protected UpdateTask(String url1, String url2) {
            this.url1 = url1;
            this.url2 = url2;
        }

        private List<PlayListEntity> sendGet() throws Exception {

            String session = null;
            String apiHeadres[][] = {{"User-Agent", USER_AGENT}, {"Connection", "Keep-Alive"}, {"Accept-Encoding", "gzip"}};

            { // get session
                URL obj = new URL(url1);
                //Log.e("LOG", "ListTorrentTv : sendGet : Url 1: " + url1);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                // optional default is GET
                con.setRequestMethod("GET");

                // add request header

                for (int i = 0; i < apiHeadres.length; i++) {
                    con.setRequestProperty(apiHeadres[i][0], apiHeadres[i][1]);
                }
                //int responseCode = con.getResponseCode();
                //Log.e("LOG", "\nSending 'GET' request to URL : " + url);
                //Log.e("LOG", "Response Code : " + responseCode);

                GZIPInputStream gzis = new
                        GZIPInputStream(con.getInputStream());
                InputStreamReader reader = new InputStreamReader(gzis);
                BufferedReader in = new BufferedReader(reader);
                StringBuilder total = new StringBuilder();
                String line;

                while ((line = in.readLine()) != null) {
                    line = line.trim();
                    total.append(line);
                }
                //Log.e("LOG", "ListTorrentTv : sendGet : Response 1 : " + total.toString());
                JSONObject jsonObject = new JSONObject(total.toString());
                session = jsonObject.getString("session");

                gzis.close();
                reader.close();
                in.close();
            }

            { // get translation

                //logo http://torrent-tv.ru/uploads/jk8kody2p38CKdj5KGXWMwRLjgFIlG.png
                // http://api.torrent-tv.ru/v3/translation_epg.php?btime=1437796800&epg_id=485&etime=1437883200&session=zE2b7mHaU9USILf3sMAe30rg&typeresult=json
                //http://api.torrent-tv.ru/v3/translation_stream.php?session=zE2b7mHaU9USILf3sMAe30rg&channel_id=1426&typeresult=json
                String url = url2.replace("#SESSIOSN#", session);
                //Log.e("LOG", "ListTorrentTv : sendGet : Url 2: " + url);
                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj
                        .openConnection();

                // optional default is GET
                con.setRequestMethod("GET");

                // add request header

                for (int i = 0; i < apiHeadres.length; i++) {
                    con.setRequestProperty(apiHeadres[i][0], apiHeadres[i][1]);
                }

                GZIPInputStream gzis = new
                        GZIPInputStream(con.getInputStream());
                InputStreamReader reader = new InputStreamReader(gzis);
                BufferedReader in = new BufferedReader(reader);
                String line;
                StringBuilder total = new StringBuilder();
                while ((line = in.readLine()) != null) {

                    total.append(line);

                }
                JSONObject jsonObject = new JSONObject(total.toString());
                //Log.e("LOG", "ListTorrentTv : sendGet : Response 2 : " + total.toString());
                //JSONArray categories=(JSONArray)jsonObject.get("categories");
                JSONArray playlist = (JSONArray) jsonObject.get("channels");
                //Log.e("torrent_tv", "playlist : " + playlist);
                for (int i = 0; i < playlist.length(); i++) {
                    JSONObject entity = (JSONObject) playlist.get(i);
                    String name = entity.getString("name");
                    String link = "http://api.torrent-tv.ru/v3/translation_stream.php?session=" + session + "&channel_id=" + entity.getString("id") + "&typeresult=json";
                    //String category=entity.getString("group");
                    String logo = "http://torrent-tv.ru/uploads/" + entity.getString("logo");
                    PlayListEntity playListEntity = new PlayListEntity(getID(), name, link, logo);
                    playListEntity.setOnDemandParser(new OnDemandParserTorrentTv());
                    playList.add(playListEntity);
                }
                //Log.e("LOG", "playList : " + playlist.length());
                gzis.close();
                reader.close();
                in.close();
            }


            return playList;
        }

        protected String doInBackground(String... urls) {
            try {
                //System.err.println("##MyTvList : doInBackground : " + urls);
                ListTorrentTv.this.parserListener.parsingStarted(getID());
                List<PlayListEntity> list = sendGet();
                ListTorrentTv.this.parserListener.parsingFinished(getID(), list);
            } catch (Exception e) {
                ListTorrentTv.this.parserListener.parsingError(getID(), new Exception("##ListTorrentTv : " + e.getMessage()));
            }

            return null;
        }

    }

}
