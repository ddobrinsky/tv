package com.nostra13.universalimageloader.sample.example;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;


public class OnDemandParserMyTv extends ParserGeneric {

    public OnDemandParserMyTv() {
        super(null);

    }

    @Override
    public void parse() {
        new UpdateTask(urls[0]).execute();
    }

    @Override
    public PlaySource getID() {
        return PlaySource.MY_TV;
    }

    private class UpdateTask extends AsyncTask<String, String, String> {

        private List<PlayListEntity> playList = new ArrayList<PlayListEntity>();
        private String url;

        protected UpdateTask(String url) {
            this.url = url;
        }

        private List<PlayListEntity> sendGet() throws Exception {

            URL obj = new URL(this.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            // optional default is GET
            con.setRequestMethod("GET");
            // add request header
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Accept-Encoding", "gzip");
            //int responseCode = con.getResponseCode();
            //Log.e("LOG", "\nSending 'GET' request to URL : " + url);
            //Log.e("LOG", "Response Code : " + responseCode);

            GZIPInputStream gzis = new GZIPInputStream(con.getInputStream());
            InputStreamReader reader = new InputStreamReader(gzis);
            BufferedReader in = new BufferedReader(reader);
            String line;
            StringBuilder total = new StringBuilder();
            while ((line = in.readLine()) != null) {
                //Log.e("LOG", "##OnDemandParserMyTv : " + line);
                total.append(line);
            }

            JSONObject jsonObject = new JSONObject(total.toString());
            String link = (jsonObject.getJSONObject("js").getString("cmd")).trim();
            playList.add(new PlayListEntity(getID(), null, link.split(" ")[1], null));
            reader.close();
            in.close();
            return playList;
        }

        protected String doInBackground(String... urls) {
            if (OnDemandParserMyTv.this.parserListener != null) {
                try {
                    OnDemandParserMyTv.this.parserListener.parsingStarted(getID());

                    List<PlayListEntity> list = sendGet();
                    OnDemandParserMyTv.this.parserListener.parsingFinished(getID(), list);
                } catch (Exception e) {
                    OnDemandParserMyTv.this.parserListener.parsingError(getID(), e);
                }
            } else {
                Log.e("LOG", "##OnDemandParserMyTv : No listener : do nothing");
            }

            return null;
        }

    }

}
