package com.nostra13.universalimageloader.sample.example;

import android.util.Log;

import com.nostra13.universalimageloader.sample.UILApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlayListEntityExtended {

    private List<String> chanelNameAlternative; //this is channel name alternatives from different APKs

    private List<PlayListEntity> playSources; //all alternative sources
    private int logoId; //logo of channel in list

    private boolean shown;
    private String channelName; //name of channel in list
    private int activeEntity = -1; //active source
    private EpgProgram epg; //link of EPG

    public PlayListEntityExtended(int logoId,boolean shown,String... names) {
        channelName = new String(names[0]);

        //for (int i = 0; i < names.length; i++) {
          //  names[i] = names[i].toLowerCase();
        //}
        this.chanelNameAlternative = Arrays.asList(names);
        this.playSources = new ArrayList<PlayListEntity>();
        this.setLogoId(logoId);
        this.setShown(shown);
    }

    public EpgProgram getEpgProgram() {
        return epg;
    }

    public void setEpgProgram(EpgProgram epg) {
        this.epg = epg;
    }

    public int getActiveEntity() {
        return activeEntity;
    }

    public void setActiveEntity(int activeEntity) {
        this.activeEntity = activeEntity;
    }

    public String getChannelName() {
        return channelName;
    }

    public int getLogoId() {
        return logoId;
    }

    public void setLogoId(int logoId) {
        this.logoId = logoId;
    }

    public boolean isShown() {
        return shown;
    }

    public void setShown(boolean shown) {
        this.shown = shown;
    }


    public void addPlaySource(PlayListEntity playListEntity) {

        if (activeEntity == -1) {
            activeEntity = 0;
        }

        playSources.add(playListEntity);
    }

    public List<PlayListEntity> getPlaySources() {
        return playSources;
    }

    public String[] getChanelNameAlternative() {
        return chanelNameAlternative.toArray(new String[chanelNameAlternative.size()]);

    }

    public JSONObject toJson(){
        JSONObject root=new JSONObject();
        try{
            JSONArray channelNames = new JSONArray(chanelNameAlternative);
            root.put("channelNames", channelNames);
            String logo=UILApplication.getInstance().getResources().getResourceEntryName(logoId);
            root.put("logo",logo);
            root.put("shown",isShown());
        }catch(Exception e){
            Log.e("LOG", "entity  : Exception : " + e);
        }
        return root;
    }



    public enum Category {
        KIDS("Kids",1,true),
        MOVIES("Movies",2,true),
        SCIENCE("Science",3,true),
        FUN("Fun",4,false),
        NEWS("News",5,false),
        MUSIC("Music",6,false),
        SPORT("Sport",7,false),
        ISRAEL("Israel",8,true),
        OTHERS("Other",9,false);


        public int getNumericShortCut() {
            return numericShortCut;
        }

        public void setNumericShortCut(int numericShortCut) {
            this.numericShortCut = numericShortCut;
        }

        private String title;   // title
        private boolean shown; // if include
        private int numericShortCut; // if include
        Category(String title,int numericShortCut, boolean shown) {
            this.title = title;
            this.shown = shown;
            this.numericShortCut = numericShortCut;
        }


        public boolean isShown() {
            return shown;
        }

        public void setShown(boolean shown) {
            this.shown = shown;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }



    }


}
