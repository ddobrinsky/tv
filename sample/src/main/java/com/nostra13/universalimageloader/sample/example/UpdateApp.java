package com.nostra13.universalimageloader.sample.example;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

/**
 * Created by Dima_BI on 8/10/15.
 */
public class UpdateApp extends AsyncTask<String, Void, Void> {
    private static UpdateApp instance = null;
    Activity context1;
    private Activity context;
    private ProgressDialog progress;

    private UpdateApp() {

    }

    public static UpdateApp newInstance() {
        //if (instance==null){
        instance = new UpdateApp();
        //}
        return instance;
    }

    public static UpdateApp getInstance() {
        return instance;
    }

    public static String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public void setContext(Activity contextf) {
        context = contextf;
    }

    @Override
    protected Void doInBackground(String... arg0) {
        context1 = context;
        context1.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progress = ProgressDialog.show(context1, "Dowloading Apk",
                        "will take time..", true);
            }
        });
        Log.e("UpdateAPP", "Start  download : " + arg0[0]);
        try {

            URL url = new URL(arg0[0]);
            final Long totalSize = Long.parseLong(arg0[1]);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(false);
            c.connect();

            String PATH = "/mnt/sdcard/Download/";
            File file = new File(PATH);
            file.mkdirs();
            File outputFile = new File(file, "update.apk");
            Log.e("UpdateAPP", "outputFile! " + outputFile);
            if (outputFile.exists()) {
                outputFile.delete();
            }
            Log.e("UpdateAPP", "FileOutputStream! ");
            FileOutputStream fos = new FileOutputStream(outputFile);

            InputStream is = c.getInputStream();
            Log.e("UpdateAPP", "getInputStream! ");
            byte[] buffer = new byte[1024];
            int len1 = 0;
            long total = 0;
            long counter = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
                total += len1;
                if (counter + 10000 < total) {
                    counter = total;

                    final String perc = ((long) (total * 100L / totalSize)) + "";
                    final String size = readableFileSize(total);
                    //Log.e("UpdateAPP", "Complete : " + perc + " %");
                    if (android.os.Build.VERSION.SDK_INT > 16) {
                        if (!context1.isDestroyed()) {

                            context1.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {


                                    progress.setMessage("Complete : " + perc + " %");
                                }

                            });
                        } else {
                            if (!context.isDestroyed()) {
                                context1 = context;
                                context1.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        progress = ProgressDialog.show(context1, "Dowloading Apk",
                                                "will take time..", true);
                                    }
                                });
                            }
                        }
                    }
                }
            }
            //Log.e("UpdateAPP", "close! ");
            fos.close();
            is.close();
            //Log.e("UpdateAPP", "Intent! ");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/update.apk")), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
            context.startActivity(intent);


        } catch (Exception e) {
            Log.e("UpdateAPP", "Update error! " + e);
        }
        context1.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    progress.dismiss();
                } catch (Exception e) {

                }
            }
        });

        return null;
    }
}