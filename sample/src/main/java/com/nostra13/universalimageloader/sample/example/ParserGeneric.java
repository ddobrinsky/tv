package com.nostra13.universalimageloader.sample.example;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.regex.Pattern;

public abstract class ParserGeneric {


    public final static String PLAY_LIST_START_ENTITY = "#EXTINF";
    public static final String TVG_ID_ATTR = "tvg-id";
    public static final Pattern TVG_ID_ATTR_REGEX = Pattern.compile(TVG_ID_ATTR + "=\"(.+?)\"");
    public static final String TVG_NAME_ATTR = "tvg-name";
    public static final Pattern TVG_NAME_ATTR_REGEX = Pattern.compile(TVG_NAME_ATTR + "=\"(.+?)\"");
    public static final String GROUP_TITLE_ATTR = "group-title";
    public static final Pattern GROUP_TITLE_ATTR_REGEX = Pattern.compile(GROUP_TITLE_ATTR + "=\"(.+?)\"");
    public static final String TVG_LOGO_ATTR = "tvg-logo";
    public static final Pattern TVG_LOGO_ATTR_REGEX = Pattern.compile(TVG_LOGO_ATTR + "=\"(.+?)\"");
    public static final String RADIO_ATTR = "radio";
    public static final Pattern RADIO_ATTR_REGEX = HlsParserUtil.compileBooleanAttrPattern(RADIO_ATTR);
    public final String USER_AGENT = "Dalvik/1.6.0 (Linux; U; Android 4.3; GT-I9505 Build/JSS15J)";
    protected ParserListener parserListener;
    protected String[] urls;

    public ParserGeneric(ParserListener parserListener) {
        setParserListener(parserListener);
    }

    public void setParserListener(ParserListener parserListener) {
        this.parserListener = parserListener;
    }

    public abstract void parse();

    public abstract PlaySource getID();

    protected  String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    protected JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    public void setUrls(String[] urls) {
        this.urls = urls;

    }

    public enum PlaySource {ISRAEL_LIST, SHARED, MY_TV, TORRENT_TV, TORRENT_STREAM, EPG,VERSION}


}
