package com.nostra13.universalimageloader.sample.example;

public interface ParserListener {
    void parsingStarted(ParserGeneric.PlaySource id);

    void parsingFinished(ParserGeneric.PlaySource id, Object obj);

    void parsingError(ParserGeneric.PlaySource id, Exception e);
}
