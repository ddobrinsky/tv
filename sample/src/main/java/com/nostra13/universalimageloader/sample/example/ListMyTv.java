package com.nostra13.universalimageloader.sample.example;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ListMyTv extends ParserGeneric {


    public ListMyTv(ParserListener parserListener) {
        super(parserListener);
        setUrls(new String[]{"http://api.freeip.tv/playlist/"});

    }

    @Override
    public void parse() {
        new UpdateTask(urls[0]).execute();
    }

    @Override
    public PlaySource getID() {
        return PlaySource.MY_TV;
    }


    private class UpdateTask extends AsyncTask<String, String, String> {

        private List<PlayListEntity> playList = new ArrayList<PlayListEntity>();
        private String url;

        protected UpdateTask(String url) {
            this.url = url;
        }

        private List<PlayListEntity> sendGet() throws Exception {

            URL obj = new URL(this.url);
            HttpURLConnection con = (HttpURLConnection) obj
                    .openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            // add request header
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Accept-Encoding", "gzip");
            InputStreamReader reader = new InputStreamReader(
                    con.getInputStream());
            BufferedReader in = new BufferedReader(reader);
            String line;
            StringBuffer playlistEntryString = new StringBuffer();
            while ((line = in.readLine()) != null) {
                line = line.trim();
                //Log.e("LOG", "line : " + line);
                if (line.startsWith(PLAY_LIST_START_ENTITY)) {
                    if (playlistEntryString.length() > 0) {
                        String[] playlistEntryArray = playlistEntryString.toString().split("#APPEND");
                        String nameString = playlistEntryArray[0];
                        String linkString = playlistEntryArray[1];


                        String tvg_id = HlsParserUtil.parseStringAttr(playlistEntryString.toString(), TVG_ID_ATTR_REGEX, TVG_ID_ATTR);
                        //System.out.println("tvg_id  : "+tvg_id);
                        String tvg_name = HlsParserUtil.parseStringAttr(playlistEntryString.toString(), TVG_NAME_ATTR_REGEX, TVG_NAME_ATTR);
                        //System.out.println("tvg_name  : "+tvg_name);
                        String group_title = HlsParserUtil.parseStringAttr(playlistEntryString.toString(), GROUP_TITLE_ATTR_REGEX, GROUP_TITLE_ATTR);
                        //System.out.println("group_title  : "+group_title);
                        String tvg_logo = HlsParserUtil.parseStringAttr(playlistEntryString.toString(), TVG_LOGO_ATTR_REGEX, TVG_LOGO_ATTR);
                        //System.out.println("tvg_logo  : "+tvg_logo);
                        ///Users/Dima_BI/Desktop/tv/plugin.video.israelive/logos/
                        ///Users/Dima_BI/Desktop/tv/pugin.xbmc.dima/logos/453391caae5b0a6fc4e80964f78a7452.png


                        boolean radio = HlsParserUtil.parseOptionalBooleanAttr(playlistEntryString.toString(), RADIO_ATTR_REGEX);
                        //System.out.println("radio  : "+radio);

                        String name = nameString.split(",")[1];
                        String group = group_title;
                        if (linkString.startsWith("#EXTGRP")) {
                            if (group == null) {
                                group = linkString.substring(9);
                            }
                            linkString = playlistEntryArray[2];
                        }
                        //http:///ch/11401
                        PlayListEntity playListEntity;
                        if (linkString.startsWith("wtfinfo://")) {
                            linkString = "http://onlinetv.allfreetv.net/stalker_portal/server/load.php?type=itv&action=create_link&cmd=" + linkString.substring(10) + "&series=&forced_storage=undefined&JsHttpRequest=139480835651139-xml";
                            playListEntity = new PlayListEntity(getID(), name, linkString, tvg_logo);
                            playListEntity.setOnDemandParser(new OnDemandParserMyTv());
                        } else {
                            playListEntity = new PlayListEntity(getID(), name, linkString, tvg_logo);
                        }


                        playList.add(playListEntity);
                        //playlistEntry.set(PlaylistEntry.PLAYLIST_METADATA, playlistEntryString.toString());
                        //Log.e("LOG", "name : " + name);
                        //Log.e("LOG", "group : " + group);
                        //Log.e("LOG", "link : " + linkString);
                        //System.out.println("linkString : "+linkString);
                    }
                    playlistEntryString = new StringBuffer();
                    playlistEntryString.append(line);
                } else {

                    if (playlistEntryString.length() > 0 && line.length() > 0) {
                        playlistEntryString.append("#APPEND");
                        playlistEntryString.append(line);
                    }
                }

            }

            reader.close();
            in.close();
            return playList;
        }

        protected String doInBackground(String... urls) {
            try {
                ListMyTv.this.parserListener.parsingStarted(getID()); //start parse event
                List<PlayListEntity> list = sendGet(); //get links
                //Log.e("LOG", "WE got total : " + list.size() + " programs");
                ListMyTv.this.parserListener.parsingFinished(getID(), list); //finish event
            } catch (Exception e) {
                ListMyTv.this.parserListener.parsingError(getID(), new Exception("##ListMyTv : " + e.getMessage()));
            }

            return null;
        }

    }

}
