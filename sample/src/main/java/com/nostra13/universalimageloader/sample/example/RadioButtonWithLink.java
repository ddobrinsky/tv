package com.nostra13.universalimageloader.sample.example;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by Dima_BI on 8/10/15.
 */
public class RadioButtonWithLink extends RadioButton {

    private int index;

    public RadioButtonWithLink(Context context) {
        super(context);
    }

    public RadioButtonWithLink(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RadioButtonWithLink(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


}
